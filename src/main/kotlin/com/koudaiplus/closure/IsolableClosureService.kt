/**
 * Copyright 2010-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.koudaiplus.closure

import com.koudaiplus.closure.mapper.ClosureDao

abstract class IsolableClosureService (
        val closureDao: ClosureDao
) : ClosureApi {

    abstract fun getTableName(): String

    override fun insertAfter(node: String, toNode: String) = closureDao.insertAfter(node, toNode, getTableName())

    override fun createRootNode(node: String) = closureDao.createRootNode(node, getTableName())

    override fun deleteNode(node: String) = closureDao.deleteNode(node, getTableName())

    override fun moveToRootNode(node: String) = closureDao.moveToRootNode(node, getTableName())

    override fun moveAfter(node: String, toNode: String) = closureDao.moveToRootNode(node, getTableName()).plus(closureDao.moveAfter(node, toNode, getTableName()))

    override fun listRoot() = closureDao.listRoot(getTableName())

    override fun listChildren(node: String) = closureDao.listChildren(node, getTableName())

    override fun getParentNode(node: String) = closureDao.getParentNode(node, getTableName())

    override fun getTopParentNode(node: String) = closureDao.getTopParentNode(node, getTableName())

    override fun getNode(node: String) = closureDao.getNode(node, getTableName())

}