/**
 * Copyright 2010-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.koudaiplus.closure

/**
 *  Closure Table api
 *
 *  Table Images
 *  ==============================
 *       A                   H
 *    B     C             I     J
 *  D  E  F  G
 *
 *  Table Records
 *  ==============================
 * ancestor descendant distance
 *    A         A         0
 *    A         B         1
 *    A         C         1
 *    A         D         2
 *    A         E         2
 *    A         F         2
 *    A         G         2
 *    B         B         0
 *    B         D         1
 *    B         E         1
 *    C         C         0
 *    C         F         1
 *    C         G         1
 *    D         D         0
 *    E         E         0
 *    F         F         0
 *    G         G         0
 *    H         H         0
 *    H         I         1
 *    H         J         1
 *    I         I         0
 *    J         J         0
 */
interface ClosureApi {

    fun insertAfter(node: String, toNode: String): Int

    fun createRootNode(node: String): Int

    fun deleteNode(node: String): Int

    fun moveToRootNode(node: String): Int

    fun moveAfter(node: String, toNode: String): Int

    fun listRoot(): List<Closure>

    fun listChildren(node: String): List<Closure>

    fun getParentNode(node: String): String?

    fun getTopParentNode(node: String): String?

    fun getNode(node: String): Closure?

}