/**
 * Copyright 2010-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.koudaiplus.closure.mapper

import com.koudaiplus.closure.Closure
import org.apache.ibatis.annotations.Delete
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select

/**
 *    CREATE TABLE `xxx_closure` (
 *       `id` bigint unsigned NOT NULL AUTO_INCREMENT,
 *       `ancestor` varchar(45) NOT NULL,
 *       `descendant` varchar(45) NOT NULL,
 *       `distance` int NOT NULL,
 *       PRIMARY KEY (`id`),
 *       UNIQUE KEY `id_UNIQUE` (`id`)
 *     ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
 *
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('A', 'A', '0');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('A', 'B', '1');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('A', 'C', '1');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('A', 'D', '2');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('A', 'E', '2');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('A', 'F', '2');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('A', 'G', '2');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('B', 'B', '0');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('B', 'D', '1');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('B', 'E', '1');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('C', 'C', '0');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('C', 'F', '1');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('C', 'G', '1');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('D', 'D', '0');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('E', 'E', '0');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('F', 'F', '0');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('G', 'G', '0');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('H', 'H', '0');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('H', 'I', '1');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('H', 'J', '1');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('I', 'I', '0');
 *    INSERT INTO `xxx_closure` (`ancestor`, `descendant`, `distance`) VALUES ('J', 'J', '0');
 */
interface ClosureDao {

    @Insert("insert into \${tableName} (`ancestor`, `descendant`, `distance`)  select `ancestor`, #{node}, `distance` + 1 from \${tableName} where descendant = #{toNode}")
    fun insertAfter(@Param("node") node: String, @Param("toNode") toNode: String, @Param("tableName")tableName: String): Int

    @Insert("insert into \${tableName} (`ancestor`, `descendant`, `distance`)  values ( #{node}, #{node}, 0)")
    fun createRootNode(@Param("node") node: String, @Param("tableName")tableName: String): Int

    @Delete("""
        delete from ${'$'}{tableName} where `ancestor` = #{node};
        delete from ${'$'}{tableName} where `descendant` = #{node};
    """)
    fun deleteNode(@Param("node") node: String, @Param("tableName")tableName: String): Int

    @Delete("delete from \${tableName} where id in (select * from (select t1.id from \${tableName} t1, \${tableName} t2 where t1.descendant = t2.descendant and t2.ancestor = #{node} and t1.ancestor in (select ancestor from \${tableName} where descendant = #{node} and distance > 0))t1);")
    fun moveToRootNode(@Param("node") node: String, @Param("tableName")tableName: String): Int

    /*
     -- 移动一个节点到另外节点之后 F移动到H下
    delete from xxx_closure where id > 0 and id in (select * from (select t1.id from xxx_closure t1, xxx_closure t2 where t1.descendant = t2.descendant and t2.ancestor = 'F' and t1.ancestor in (select ancestor from xxx_closure where descendant = 'F' and distance > 0))t1);
    insert into xxx_closure (`ancestor`, `descendant`, `distance`) select 'H', descendant, `distance` + 1 from xxx_closure where ancestor = 'F';
    insert into xxx_closure (`ancestor`, `descendant`, `distance`) select t1.ancestor, t2.descendant, t1.distance + t2.distance from (select * from xxx_closure where descendant = 'H' and distance > 0) t1, xxx_closure t2 where t2.ancestor = 'H' and t2.distance > 0;
     */
    @Insert("""
        insert into ${'$'}{tableName} (`ancestor`, `descendant`, `distance`) select #{toNode}, descendant, `distance` + 1 from ${'$'}{tableName} where ancestor = #{node};
        insert into ${'$'}{tableName} (`ancvestor`, `descendant`, `distance`) select t1.ancestor, t2.descendant, t1.distance + t2.distance from (select * from ${'$'}{tableName} where descendant = #{toNode} and distance > 0) t1, ${'$'}{tableName} t2 where t2.ancestor = #{toNode} and t2.distance > 0;
    """)
    fun moveAfter(@Param("node") node: String, @Param("toNode") toNode: String, @Param("tableName")tableName: String): Int

    @Select("""
        select * from ${'$'}{tableName} t1 where descendant = ancestor and not exists (
            select 1 from ${'$'}{tableName} t2 where t1.ancestor = t2.descendant and t2.distance > 0
        )
    """)
    fun listRoot(@Param("tableName")tableName: String): List<Closure>

    @Select("select * from \${tableName} where ancestor = #{node}")
    fun listChildren(@Param("node") node: String, @Param("tableName")tableName: String): List<Closure>

    @Select("select ancestor from \${tableName} where descendant = #{node} and distance = 1")
    fun getParentNode(@Param("node") node: String, @Param("tableName")tableName: String): String?

    @Select("select ancestor from \${tableName} where descendant = #{node} and distance > 0 order by distance desc limit 1")
    fun getTopParentNode(@Param("node") node: String, @Param("tableName")tableName: String): String?

    @Select("select * from \${tableName} where ancestor = descendant and ancestor = #{node}")
    fun getNode(@Param("node") node: String, @Param("tableName")tableName: String): Closure?

}